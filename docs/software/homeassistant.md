# Home Assistant

[Home Assistant](https://www.home-assistant.io/) can automate just about any part of your home.

## Access

It is available at [http://homeassistant.{{ domain }}/](http://homeassistant.{{ domain }}/)