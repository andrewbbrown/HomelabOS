# NextCloud

[NextCloud](https://nextcloud.com/) is your Dropbox / Google Calendar replacement.

## Access

It is available at [http://nextcloud.{{ domain }}/](http://nextcloud.{{ domain }}/)